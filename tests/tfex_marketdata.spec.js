import { expect, test } from "@playwright/test";
test.use({ storageState: "userTFEX.json" });
// test("TFEX-MKDT-DMQ-002", async ({ page }) => {
//   await page.goto(
//     "https://dev2.tfex.co.th/th/market-data/daily-market-quotation/block-trade",
//     {
//       timeout: 60 * 1000,
//       waitUntil: "domcontentloaded",
//     }
//   );
//   const titleCoverpage = "ข้อมูลการซื้อขายแบบรายใหญ่";
//   console.log(await page.locator("css=h1.cover-page-title").innerText());
//   await expect(page.locator("css=h1.cover-page-title")).toHaveText(
//     titleCoverpage
//   );
//   await page.locator(".group-title").nth(0).click();
//   await page.locator(".group-title").nth(0).click();
//   await page.pause();
// });
test("TFEX-MKDT-MKRP-002", async ({ page, context }) => {
  let yearDefeult;
  let yearSelect;
  await page.goto(
    "https://dev2.tfex.co.th/th/market-data/historical-data/monthly-market-report",
    {
      timeout: 60 * 1000,
      waitUntil: "domcontentloaded",
    }
  );
  const titleCoverpage = "รายงานการซื้อขายรายเดือน";
  console.log(await page.locator("css=h1.cover-page-title").innerText());
  await expect(page.locator("css=h1.cover-page-title")).toHaveText(
    titleCoverpage
  );
  yearDefeult = await page.locator(".input-select-value").innerText();
  console.log("yearDefeult : " + yearDefeult);
  await page.locator(".input-select-value").click();
  await page.locator(".input-select-item-text").nth(4).click();
  yearSelect = await page.locator(".input-select-item-text").nth(4).innerText();
  console.log("yearSelect : " + yearSelect);
  await page.waitForTimeout(3000);
  expect(yearDefeult).not.toEqual(yearSelect);
  await page.locator(".col-6.col-md-3.pe-md-0").nth(0).click();
  const page2 = await context.waitForEvent("page");
  await page2.waitForLoadState();
  //expect(await page2.title()).toContain("TFEX");
  await expect(page2).toHaveURL(/https/);
  console.log(page2.url());
  // const pagePromise = context.waitForEvent("page");
  // await page.locator(".col-6.col-md-3.pe-md-0").nth(0).click();
  // const newPage = await pagePromise;
  // await newPage.waitForLoadState();
  // console.log(await newPage.title());
  // expect(await page2.locator('id=title')).toContain("TFEX");
  //await page.pause();
});
test("TFEX-MKDT-MKRP-010", async ({ page }) => {
  await page.goto(
    "https://dev2.tfex.co.th/th/market-data/reference-data/metal",
    {
      timeout: 60 * 1000,
      waitUntil: "domcontentloaded",
    }
  );
  const breadcrumb = "Metal";
  console.log(await page.locator("css=li.breadcrumb-item").nth(3).innerText());
  await expect(page.locator("css=li.breadcrumb-item").nth(3)).toHaveText(
    breadcrumb
  );

  await page.pause();
});
