// import { test, chromium } from "@playwright/test";

// async function genUser2() {
//   const browser = await chromium.launch({
//     headless: false,
//     args: ["--disable-blink-features=AutomationControlled"],
//   });
//   const context = await browser.newContext({});
//   const page = await context.newPage();
//   await page.goto("https://newerp.clicknext.com/login");
//   await page.waitForTimeout(5000);
//   page.evaluate("window.location.reload();"), { timeout: 60 * 1000 };
//   await page
//     .frameLocator('iframe[title="ปุ่มลงชื่อเข้าใช้ด้วย Google"]')
//     .locator("id=container")
//     .click();
//   const page1 = await page.waitForEvent("popup");
//   await page1.waitForSelector('input[type="email"]');
//   await page1.type('input[type="email"]', "porramet.a@clicknext.com");
//   await page1.click("#identifierNext");
//   await page1.waitForSelector('input[type="password"]');
//   await page1.type('input[type="password"]', "Met22165@");
//   await page1.waitForSelector("#passwordNext");
//   await page1.click("#passwordNext");
//   await page.context().storageState({ path: "genUser2.json" });
//   await page.waitForTimeout(5000);
// }
// test("GET JSON Porramet", async ({}) => {
//   await genUser2();
// });
import { test, chromium, expect } from "@playwright/test";
// test.use({ storageState: "storageState.json" });
test("valid login", async ({}) => {
  const browser = await chromium.launch({
    headless: false,
    args: ["--disable-blink-features=AutomationControlled"],
  });
  const context = await browser.newContext({});

  const page = await context.newPage();
  await page.goto(
    "https://newerp.clicknext.com/budget-management/financial-projection"
  );
  await page.waitForTimeout(2000);
  page.evaluate("window.location.reload();"), { timeout: 60 * 1000 };
  await page.waitForTimeout(2000);
  await page
    .frameLocator('iframe[title="ปุ่มลงชื่อเข้าใช้ด้วย Google"]')
    .locator("id=container")
    .click();
  const page1 = await page.waitForEvent("popup");
  await page1.waitForSelector("input[type=email]");
  await page1.locator("input[type=email]").type("porramet.a@clicknext.com");
  await page1.locator("id=identifierNext").click();
  await page1.waitForSelector("input[type=password]");
  await page1.locator("input[type=password]").type("Met22165@");
  await page1.locator("id=passwordNext").click();
  await expect(page).toHaveURL(
    "https://newerp.clicknext.com/budget-management/financial-projection"
  );
  await page.context().storageState({ path: "genUser2.json" });
});
