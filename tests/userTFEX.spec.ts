import { test, chromium, expect } from "@playwright/test";
test("valid login TFEX", async ({}) => {
  const browser = await chromium.launch({
    headless: false,
    args: ["--disable-blink-features=AutomationControlled"],
  });
  const context = await browser.newContext({});

  const page = await context.newPage();
  await page.goto(
    "https://classictest.set.or.th/set-member/login?system=TFEX_DEV&channel=WEB&language=th&redirectUrl=https://dev2.tfex.co.th/th/home"
  );
  await page.waitForTimeout(2000);
  page.evaluate("window.location.reload();"), { timeout: 60 * 1000 };
  await page.waitForTimeout(2000);
  await page.getByPlaceholder("ชื่อผู้ใช้").click();
  await page.getByPlaceholder("ชื่อผู้ใช้").type("thanet9927@gmail.com");
  await page.getByPlaceholder("รหัสผ่าน").click();
  await page.getByPlaceholder("รหัสผ่าน").type("Taneattadsab254");
  await page.locator(".button-submit").click();
  await expect(page).toHaveURL("https://dev2.tfex.co.th/th/home");
  await page.context().storageState({ path: "userTFEX.json" });
});
