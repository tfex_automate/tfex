import { test } from "@playwright/test";
test("Login Taneat", async ({ browser }) => {
  const context = await browser.newContext({
    storageState: "genUser1.json",
  });
  //await genUser1();
  const page = await context.newPage();
  await page.context().storageState({ path: "genUser1.json" });
  await page.goto(
    "https://newerp.clicknext.com/budget-management/financial-projection"
  );
  await page.waitForTimeout(10000);
});

test("Login Porramet", async ({ browser }) => {
  const context = await browser.newContext({
    storageState: "genUser2.json",
  });
  const page = await context.newPage();
  // await page.context().storageState({ path: "genUser2.json" });
  await page.goto(
    "https://newerp.clicknext.com/budget-management/financial-projection"
  );
  await page.waitForTimeout(10000);
});
