import { chromium, FullConfig } from "@playwright/test";

async function gobalSetup(config: FullConfig) {
  const browser = await chromium.launch({
    headless: false,
    args: ["--disable-blink-features=AutomationControlled"],
  });
  const context = await browser.newContext({});
  const page = await context.newPage();
  await page.setDefaultNavigationTimeout(0);
  await page.goto(
    "https://accounts.google.com/signin/v2/identifier?hl=en&flowName=GlifWebSignIn&flowEntry=ServiceLogin"
  );
  //await page.pause();
  await page.waitForSelector('input[type="email"]');
  await page.type('input[type="email"]', "thanet9927@gmail.com");
  await page.click("#identifierNext");
  await page.waitForSelector('input[type="password"]');
  await page.type('input[type="password"]', "taneattadsab254");
  await page.waitForSelector("#passwordNext");
  await page.click("#passwordNext");
  await page
    .context()
    .storageState({ path: config.projects[0].use.storageState as string });
  await page.waitForTimeout(3000);
  // await browser.close();
}
export default gobalSetup;
